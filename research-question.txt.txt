Semantic similarity for Image and Text for captions
Type of Research question: <late>

The past 15 years, We have seen a dramatic advances in computational power and connectivity that led to a exponential rise in image captioning, a problem which a computational algorithm, which writes description for images, Most of its progress has been seen to extensive improvement in the Deep Learning algorithms in both computer vision and Natural Language processing, with rapid rise of cheap and large datasets. In addition to mainstream applications, such as providing captions of images for visually impaired people, These datasets also enable us to understand into exciting research questions about grounding language and inputs. For example representations for a word like "car", means using and understanding both linguistic and visual context. 
PICO
Problem
"Image captioning data sets are available in huge numbers in big tech companies, popular image datasets like MS-COCO and Flickr30k were widely used to understand and represent the the aligned images and text representation and to build captioning models. But the real problem arises here, There is no cross model association, Images are not paired with other images, captions are only paired with other captions of the same image (also called co captions)," there are image caption pairs, that match but are not labeled as much, and there are no labels that indicate where and image caption pair doesn't match. "This undermines research into how inter modality learning (connecting captions to images, for example) impacts intra modality tasks connecting captions to captions or images to images This is important to address, especially because a fair amount of work on learning from images paired with text is motivated by arguments about how visual elements should inform and improve representations of language."

Intervention
To address this issue of evaluation gap, I propose a Crisscrossed captions, Extended Intra modal and Inter moda semantic similarity judgements for" MS coco,which is very recently presented at European Chapter of Association for computational linguistcs 2021. This crisscrossed captions data sets extend the development I the area of test splits with semantic similarity for image text, text-text and image- image pairs. This rating criteria is quiet popular which is completely based on semantic textual similarity, which is widely adopted measure of semantic" relatedness between pairs of short texts, which we extend to include judgments about images as well.  This method is basically adding,existing "MSCOCO evaluation sets by adding human derived semantic similarity ratings for existing image caption pairs and co captions which are solid lines as shown in figure, and it increases the rating density by adding human ratings for new image caption , caption-caption and image-image pairs as dashed lines."

Comparison
For comparison, I have collected a series of results using bert based text encoder and efficient net b4 as image encoder. For the first solution, model uses a shared text encoder on both sides, for the second one, an image to text model . that is more aforementioned text and image encoders, and include a layer above the text encoder to match the images of encoder output.the third being a  multi task model which is heavilty trained on weighted combination of text to text and image to text tasks and the comaprisions which are collected are provided in the reference below.



Outcomes

This problem tries to solve the main issue for visually impaired people by providing description of images, which is a huge application on the long run, work is extensively done in this topic.

